$(document).ready(() => {
    if(isTouchDevice()) changeClasses();
});

function isTouchDevice() {
    return !!('ontouchstart' in window) || !!('msmaxtouchpoints' in window.navigator);
}

function changeClasses() {
    $('.box').each(function(){
        $(this).removeClass('box');
        $(this).addClass('box-touch');
    });
}