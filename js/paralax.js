window.addEventListener("DOMContentLoaded", this.scrollLoop, false);
let css = {};
let photosT = $('#photos').position().top;
let photosL = $('#photos').position().left;
function scrollLoop(e){

    //main paralax
    y = window.scrollY/3;
    let opacity = 1 - y/100;
    css = {
        transform: `translateY(-${y}px)`,
        opacity: `${opacity}`
    }
    $('.main').css(css);

    //photos paralax
    let photosH = $('#photos').height();
    if(photosH < window.scrollY) {
        opacity = 1 - (window.scrollY - photosH)/500;
        $('#photos').css({
            transform: `translateY(${100 - (window.scrollY - photosH)/20}vh)`,
            opacity: `${opacity}`
        })
    } else {
        $('#photos').css({
            transform: `translateY(${100 - (window.scrollY - photosH)/30}vh)`,
            opacity: `1`
        })
    }

    //contact opacity
    opacity_con = (window.scrollY - $('#contact').height())/($('#contact').position().top - $('#contact').height())*1.5 - 0.5;
    $('#contact').css('opacity', opacity_con);

    
    requestAnimationFrame(scrollLoop);
};